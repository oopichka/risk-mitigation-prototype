import { Button } from "@/client/components/ui/button"
import { useAppContext } from "@/client/context/Context"
import { cn } from "@/client/lib/utils"

export { Link }

function Link(props: {
  href?: string
  className?: string
  children: React.ReactNode
}) {
  const { location } = useAppContext()
  // const className = [props.className, pageContext.urlPathname === props.href && 'is-active'].filter(Boolean).join(' ')
  const isActive = location === props.href
  const className = [props.className].filter(Boolean).join(" ")

  return (
    <Button
      asChild={!isActive}
      variant={isActive ? "outline" : "link"}
      disabled
      className={cn(!isActive ? "border border-transparent" : "")}
    >
      {isActive ? (
        props.children
      ) : (
        <a href={props.href} className={className}>
          {props.children}
        </a>
      )}
    </Button>
  )
}

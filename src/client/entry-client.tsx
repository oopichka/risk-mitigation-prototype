import React from "react"
import ReactDOM from "react-dom/client"

import { BrowserRouter } from "react-router-dom"
import "../client/styles/shadcn.css"
import { App } from "./App"

ReactDOM.hydrateRoot(
  document.getElementById("root") as HTMLDivElement,
  <React.StrictMode>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </React.StrictMode>
)

import React, { useContext, useEffect, useState } from "react"

export interface Context {
  name: string
  location: string
  setName: (val: string) => void
}
const defaultVal = {
  name: "",
  location: "/",
  setName: () => {},
} as Context

const context = React.createContext(defaultVal)

const { Provider } = context

export const ContextWrapper = ({ children }: { children: any }) => {
  const [location, setLocation] = useState(defaultVal.location)
  const [name, setName] = useState(defaultVal.name)

  useEffect(() => {
    setLocation(window.location.pathname)
  }, [])

  return (
    <>
      <Provider
        value={{
          name,
          location,
          setName,
        }}
      >
        {children}
      </Provider>
    </>
  )
}

export const useAppContext = () => useContext(context)

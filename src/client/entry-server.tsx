import React from "react"
import ReactDOM from "react-dom/server"
import { StaticRouter } from "react-router-dom/server"
import "../client/styles/shadcn.css"
import { App } from "./App"

export const render = (url: string) => {
  return ReactDOM.renderToString(
    <React.StrictMode>
      <StaticRouter location={url}>
        <App />
      </StaticRouter>
    </React.StrictMode>
  )
}

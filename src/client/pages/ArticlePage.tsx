import { Link } from "@/client/components/Link"
import { ChevronLeft, ChevronRight } from "lucide-react"
import React, { useState } from "react"
import { Button } from "../components/ui/button"
import { Card, CardHeader } from "../components/ui/card"
import { Separator } from "../components/ui/separator"
import { useAppContext } from "../context/Context"
import { cn } from "../lib/utils"

export const ArticlePage = () => {
  const { name, location, setName } = useAppContext()
  const [page, setpage] = useState(0)

  const PageBTNs = () => {
    return (
      <div className="flex flex-row items-center justify-center">
        <div className="flex flex-row items-center gap-2 max-w-[150px] gap-5">
          <div>
            <Button
              variant={page <= 1 ? "ghost" : "default"}
              size={"icon"}
              disabled={page <= 1}
              className="flex items-center justify-center"
              onClick={(e) => setpage(page - 5)}
              aria-label={"previous set of articles"}
            >
              <ChevronLeft className="h-4 w-4" />
            </Button>
          </div>
          <>{page / 5 + 1}/5</>
          <div>
            <Button
              variant={page >= 20 ? "ghost" : "default"}
              size={"icon"}
              disabled={page >= 20}
              className="flex items-center justify-center"
              onClick={(e) => setpage(page + 5)}
              aria-label={"next set of articles"}
            >
              <ChevronRight className="h-4 w-4" />
            </Button>
          </div>
        </div>
      </div>
    )
  }

  return (
    <div className="container flex flex-col items-center ">
      <div className="flex flex-col gap-3 max-w-[1000px] mb-5">
        <div className="flex flex-row justify-between items-center">
          <h1 className="text-3xl font-extrabold tracking-tight">Articles</h1>
          <PageBTNs />
        </div>
        <Separator className="mb-2" />
        {[page + 1, page + 2, page + 3, page + 4, page + 5].map((element) => {
          const key = `article-isca-${element}`
          return (
            <Card key={key}>
              <CardHeader className="flex sm:flex-col md:flex-row gap-4 ">
                <div className="md:w-[200px] md:max-w-[200px] md:max-h-hull sm:w-full md:max-w-hull sm:max-h-[200px]">
                  <img
                    src={`https://picsum.photos/seed/${key}/500`}
                    alt={`Image for Article ${element}`}
                    width={500}
                    height={500}
                    className={cn(
                      "rounded-md object-cover ",
                      "md:w-[150px] md:max-w-[150px] md:max-h-hull",
                      "max-md:w-full max-md:max-w-hull max-md:max-h-[200px]"
                    )}
                  />
                </div>
                <div className="w-full">
                  <h2 className="text-2xl font-semibold tracking-tight">
                    Article {element}
                  </h2>
                  <Separator className="my-2" />
                  <p>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                    Quis, provident at tempore eius commodi autem ipsam rem
                    fugit debitis, officiis consequatur officia cupiditate ut
                    nemo voluptas maiores est eos tenetur! Saepe aut corporis
                    eveniet vitae, commodi qui rem ipsa, voluptates optio
                    officia quae ea inventore facere, possimus illo
                    reprehenderit! Eaque!
                  </p>
                </div>
              </CardHeader>
            </Card>
          )
        })}
        <Separator className="my-2" />
      </div>
    </div>
  )
}

import React from "react"
import { Link } from "./Link"

export const Navbar = () => {
  return (
    <header className="z-[100] h-[55px] bg-white/50 items-center fixed top-0 w-full flex flex-row justify-between px-7 py-2 border-b backdrop-blur-lg">
      <h1 className="text-xl font-extrabold tracking-tight select-none">
        Logo
      </h1>
      <nav className="flex flex-row gap-2">
        <Link href="/">Home</Link>
        <Link href="/articles">Articles</Link>
        <Link href="/about">About</Link>
        <Link href="/contact">Contact</Link>
      </nav>
    </header>
  )
}

import { Link } from "@/client/components/Link"
import { ChevronLeft, ChevronRight } from "lucide-react"
import React, { useState } from "react"
import logo from "../../../static/logo.svg"
import { Button } from "../components/ui/button"
import { Card, CardHeader } from "../components/ui/card"
import { Separator } from "../components/ui/separator"
import { useAppContext } from "../context/Context"
import { cn } from "../lib/utils"
import "../styles/HomePage.css"

export const AboutPage = () => {
  const { name, location, setName } = useAppContext()
  const [count, setCount] = useState(0)

  return (
    <div>
      <h1 className="scroll-m-20 text-4xl font-extrabold tracking-tight lg:text-5xl">
        Articles
      </h1>
    </div>
  )
}

import { ContextWrapper } from "@/client/context/Context"
import DefualtLayout from "@/client/layout/defualt"
import { AboutPage } from "@/client/pages/AboutPage"
import { HomePage } from "@/client/pages/HomePage"
import { NotFoundPage } from "@/client/pages/NotFoundPage"
import { Route, Routes } from "react-router-dom"
import "../client/styles/shadcn.css"
import { ArticlePage } from "./pages/ArticlePage"
import { ContactPage } from "./pages/ContactPage"

export const App = () => {
  return (
    <ContextWrapper>
      <DefualtLayout>
        <Routes>
          <Route path="/" element={<HomePage />} />
          <Route path="/about" element={<AboutPage />} />
          <Route path="/articles" element={<ArticlePage />} />
          <Route path="/contact" element={<ContactPage />} />
          <Route path="*" element={<NotFoundPage />} />
        </Routes>
      </DefualtLayout>
    </ContextWrapper>
  )
}

import { Footer } from "@/client/components/Footer"
import { Navbar } from "@/client/components/Navbar"
import "@/client/styles/shadcn.css"
const DefualtLayout = ({ children }: { children: any }) => {
  return (
    <div className="App">
      <Navbar />
      <main className="mt-[55px] p-3 h-full">{children}</main>
      <Footer />
    </div>
  )
}

export default DefualtLayout

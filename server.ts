import { Request, Response } from "express"
import routes from "./src/server/routes"

const fs = require("fs")
const path = require("path")
const express = require("express")
const robots = require("express-robots-txt")

const isTest = process.env.NODE_ENV === "test" || !!process.env.VITE_TEST_BUILD

async function createServer(
  root = __dirname,
  isProd = process.env.NODE_ENV === "production"
) {
  // Contructs an absolute file path relative to this file.
  const resolve = (p: string) => path.resolve(__dirname, p)
  // If this is production grab the
  const indexProd = isProd
    ? fs.readFileSync(resolve("client/index.html"), "utf-8")
    : ""

  const app = express()
  app.use(express.json())

  // The route to use ase the base for all our api routes.
  app.use("/api", routes)

  // Passing in the assets folder for static assets.
  const requestHandler = express.static(resolve("assets"))
  app.use(requestHandler)
  app.use("/assets", requestHandler)

  // Creates the robots.txt file to allow search engines to crawl the site.
  app.use(
    robots({
      UserAgent: "*",
      Allow: "/",
    })
  )
  //-------------------------------------------------------------------------
  // Set up of the Development Vite Server. Will listen to the client
  //  and if the code is updated it will refresh the changes in the browser.
  /**
   * @type {import('vite').ViteDevServer}
   */
  let vite: any
  if (!isProd) {
    vite = await require("vite").createServer({
      root,
      logLevel: isTest ? "error" : "info",
      server: {
        middlewareMode: "ssr",
        watch: {
          // During tests we edit the files too fast and sometimes chokidar
          // misses change events, so enforce polling for consistency
          usePolling: true,
          interval: 100,
        },
      },
    })
    // use vite's connect instance as middleware
    app.use(vite.middlewares)
  } else {
    app.use(require("compression")())

    app.use(
      require("serve-static")(resolve("dist/client"), {
        index: false,
      })
    )
  }
  //-------------------------------------------------------------------------

  // The catch all route, will take this route, Pull in the entry-server for
  //   react, and return the to the user the heml and other static files.
  app.use("*", async (req: Request, res: Response) => {
    try {
      const url = req.originalUrl

      let template, render
      if (!isProd) {
        // always read fresh template in dev
        template = fs.readFileSync(resolve("index.html"), "utf-8")
        template = await vite.transformIndexHtml(url, template)
        render = (await vite.ssrLoadModule("/src/client/entry-server.tsx"))
          .render
      } else {
        template = indexProd
        render = require("./server/entry-server.js").render
      }

      const context = {}
      const appHtml = render(url, context)

      const html = template.replace(`<!--root-html-->`, appHtml)

      res.status(200).set({ "Content-Type": "text/html" }).end(html)
    } catch (e: any) {
      !isProd && vite.ssrFixStacktrace(e)
      console.log(e.stack)
      res.status(500).end(e.stack)
    }
  })

  return { app, vite }
}

createServer().then(({ app }) => {
  const port = process.env.PORT || 4000
  app.listen(Number(port), "0.0.0.0", () => {
    console.log(`App is listening on http://localhost:${port}`)
  })
})

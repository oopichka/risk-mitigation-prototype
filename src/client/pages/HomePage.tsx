import { Link } from "@/client/components/Link"
import React, { useState } from "react"
import logo from "../../../static/logo.svg"
import { useAppContext } from "../context/Context"

export const HomePage = () => {
  const { name, location, setName } = useAppContext()
  const [count, setCount] = useState(0)

  return (
    <div>
      <h1 className="scroll-m-20 text-4xl font-extrabold tracking-tight lg:text-5xl">
        Home
      </h1>
    </div>
  )
}
